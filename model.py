from flask import Flask
from marshmallow import Schema, fields
from flask_marshmallow import Marshmallow
from flask_sqlalchemy import SQLAlchemy


ma = Marshmallow()
db = SQLAlchemy()


class DatetimeEvent(db.Model):
    __tablename__ = 'events'
    id = db.Column(db.Integer, primary_key=True, autoincrement=True, unique=True, nullable=False)
    event = db.Column(db.String(250), nullable=False)
    date = db.Column(db.DateTime, nullable=False)

    def __init__(self, event, date):
        self.event = event
        self.date = date


class DatetimeEventSchema(ma.Schema):
    id = fields.Integer(dump_only=True)
    event = fields.String(required=True)
    date = fields.DateTime(required=True)