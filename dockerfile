FROM python:3.7-alpine

RUN mkdir /app

COPY . /app/

ENV FLASK_HOST "0.0.0.0"

WORKDIR /app

RUN pip install -r requirements.txt
RUN python migrate.py db init
RUN python migrate.py db migrate
RUN python migrate.py db upgrade


ENTRYPOINT [ "python", "run.py" ]
