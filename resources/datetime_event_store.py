from flask import request, jsonify, make_response
from flask_restful import Resource, reqparse
from model import db, DatetimeEvent, DatetimeEventSchema
import datetime
import random

datetime_events_schema = DatetimeEventSchema(many=True)
datetime_event_schema = DatetimeEventSchema()


class DatetimeEventStore(Resource):
    def get(self):
        parser = reqparse.RequestParser()
        parser.add_argument('start', type=str)
        parser.add_argument('end', type=str)
        args = parser.parse_args()

        if args["start"] is None and args["end"] is None:
            events = DatetimeEvent.query.all()
            events = datetime_events_schema.dump(events)
            return jsonify({'status': 'success', 'data': events})
        elif args["start"] is not None and args["end"] is not None:
            return self.get_events(args["start"], args["end"])
        else:
            return make_response({'status': 'failed', 'error': "You cannot set one of URL parameters, you must set \"start\" and \"end\" or neither."}, 400)

    def get_events(self, start, end):
        events = DatetimeEvent.query.filter(
            DatetimeEvent.date >= start, DatetimeEvent.date <= end)
        events = datetime_events_schema.dump(events)
        return jsonify({'status': 'success', 'data': events})

    def validate_json_data(self, json_data):
        if not json_data:
            return {'status': 'failed', 'error': 'No input data provided'}, 400
        else:
            if "date" in json_data.keys():
                try:
                    datetime.datetime.strptime(
                        json_data["date"], "%Y-%m-%dT%H:%M:%S")
                except:
                    return {'status': 'failed', 'error': 'Not valid data. "date" must be in "YYYY-MM-DDTHH:MM:SS" format.'}, 400
            else:
                return {'status': 'failed', 'error': 'Not valid data. "date" is required.'}, 400

        errors = datetime_event_schema.validate(json_data)
        if errors:
            return errors, 422
        return json_data, None

    def post(self):
        parser = reqparse.RequestParser()
        parser.add_argument('random', type=bool)
        args = parser.parse_args()

        json_data = request.get_json(force=True)
        print(json_data)

        if not args or not args["random"]:
            json_data, errCode = self.validate_json_data(json_data)
            if errCode is not None:
                return make_response(json_data, errCode)
            data = self.store_event(datetime.datetime.strptime(
                json_data["date"], "%Y-%m-%dT%H:%M:%S"), json_data["event"])
            return make_response({"status": 'success', 'data': data}, 201)
        else:
            if not json_data:
                return make_response({'status': 'failed', 'error': 'No input data provided'}, 400)
            else:
                for key in json_data.keys():
                    if key in ["start_date", "end_date"]:
                        try:
                            datetime.datetime.strptime(json_data[key], "%Y-%m-%dT%H:%M:%S")
                        except:
                            return make_response({'status': 'failed', 'error': 'Not valid data. "%s" must be in "YYYY-MM-DDTHH:MM:SS" format.' % key}, 400)
                if "nbevent" not in json_data.keys(): 
                    return make_response({'status': 'failed', 'error': 'Not valid data. "nbevent" must be an integer between 0 and 10000.'}, 400)
                else:
                    try:
                        int(json_data["nbevent"])
                    except:
                        return make_response({'status': 'failed', 'error': 'Not valid data. "%s" must be an integer between 0 and 10000.'}, 400)
                if "event" not in json_data.keys(): 
                    return make_response({'status': 'failed', 'error': 'Not valid data. "event" must be set.'}, 400)
                start_ts = datetime.datetime.strptime(json_data["start_date"], "%Y-%m-%dT%H:%M:%S").timestamp()
                end_ts = datetime.datetime.strptime(json_data["end_date"], "%Y-%m-%dT%H:%M:%S").timestamp()
                if start_ts > end_ts:
                    return make_response({'status': 'failed', 'error': '"Start_date" cannot be higher than "End_date"'}, 400)
                events = []
                for i in range(int(json_data["nbevent"])):
                    date = datetime.datetime.fromtimestamp(random.randint(start_ts, end_ts))
                    events.append(self.store_event(date, "%s_%d" % (json_data["event"], i)))
                return make_response({'status': 'success', 'data': events}, 201)

                
    def store_event(self, at, event):
        e = DatetimeEvent(event, at)
        db.session.add(e)
        db.session.commit()
        return datetime_event_schema.dump(e)

    def put(self, id):
        event = DatetimeEvent.query.filter_by(id=id).first()
        if not event:
            return make_response({'status': 'failed', 'error': 'No event found.'}, 404)
        else:
            json_data = request.get_json(force=True)
            json_data, errCode = self.validate_json_data(json_data)
            if errCode is not None:
                return make_response(json_data, errCode)
            else:
                event.event = json_data["event"]
                event.date = datetime.datetime.strptime(
                    json_data["date"], "%Y-%m-%dT%H:%M:%S")
                db.session.add(event)
                db.session.commit()
            return jsonify({'status': 'success', 'data': datetime_event_schema.dump(event)})

    def delete(self, id):
        event = DatetimeEvent.query.filter_by(id=id).first()
        if not event:
            return make_response({'status': 'failed', 'error': 'No event found.'}, 404)
        else:
            db.session.delete(event)
            db.session.commit()
            return make_response("Event %s deleted." % id, 200)
