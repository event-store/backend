# DATETIME EVENTS STORE

I made the choice to do the package as an API, because it's the easiest way for me to add an HMI, and it's current technology that's used very often.
Plus I've been trying to get closer to what you might ask me if I get the job.

I've use Python3.7 with Flask framework for the Backend server and ReactJS for the Frontend which I've deploy in my Backend server

## Installation

I purpose 2 way for install it.

### In comand line

Setup environment with a virtualenv 

```bash
git clone https://gitlab.com/event-store/backend.git
cd backend
python3.7 -m venv env
source env/bin/activate
pip install -r requirements.txt
```

Create database and table

```bash
python migrate.py db init
python migrate.py db migrate
python migrate.py db upgrade
```

Launch server on localhost:5000 by default, if you want change the host, you can set it with environment varible.

```bash
export FLASK_HOST="0.0.0.0" # for example
python run.py
```

### With Docker

```bash
git clone https://gitlab.com/event-store/backend.git
cd backend
docker build -t datetime_event_store_img:latest .
docker run -d -p 80:5000 datetime_event_store_img --name datetimeEventStore_app
```

## Next Steps

1) Add checks and validations on Form
2) Add error messages in HMI when something wrong in request to backend
3) Add Button for change ordrer of events list
4) Add the possibility to remove a list of events 

5) Add authentication

## Timing

I did this project in almost 13 hours, I clocked the start and end time each time.