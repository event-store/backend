from flask import Blueprint
from flask_restful import Api
from resources.datetime_event_store import DatetimeEventStore

api_bp = Blueprint('api', __name__)
api = Api(api_bp)

# Route
api.add_resource(DatetimeEventStore, '/events', '/events/<int:id>', endpoint="events")