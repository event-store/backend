from flask import Flask, render_template


def create_app(config_filename):
    app = Flask(__name__)
    app.config.from_object(config_filename)

    @app.route('/', defaults={'path': ''})
    @app.route('/<path:path>')
    def homepage(path):
        return render_template('index.html')

    from app import api_bp
    app.register_blueprint(api_bp, url_prefix='/api')

    from model import db
    db.init_app(app)

    return app


if __name__ == "__main__":
    app = create_app("config")
    app.run(debug=True, host=app.config["HOST"], port=app.config["PORT"])
